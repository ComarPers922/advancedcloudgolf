﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void StateUpdateHandler();
public delegate bool StateTransitionConditionHandler();
public delegate void TransitionHandler();
public interface StateTransition
{
    StateTransitionConditionHandler CheckTransitionCondition { get; }
    TransitionHandler DoTransition { get; }
}
public interface State
{
    string Name { get; }
    StateUpdateHandler Update { get; }
    Dictionary<StateTransition, State> NextStates { get; }
    void AddTransition(State to, StateTransition condition);
    State CheckTransition();
}
