﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class PostProcessingCamera : MonoBehaviour
{
    [SerializeField]
    private Shader VignetteShader;
    [SerializeField]
    private Shader BlurShader;

    private Material vignetteMaterial;
    private Material blurMaterial;
    private Camera camera;

    [SerializeField, Range(0, 1)]
    private float Intensity = 0;
    [SerializeField, Range(0, 0.02f)]
    private float BlurRadius = 0.125f;
    [SerializeField, Range(0, 10)]
    private int Iterations = 3;
    // Start is called before the first frame update
    void Start()
    {
        // texture = new RenderTexture(Screen.width, Screen.height, -1);
        vignetteMaterial = new Material(VignetteShader);
        blurMaterial = new Material(BlurShader);
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        //RenderTexture tempTexture = new RenderTexture(source);
        //RenderTexture secondTempTexture = new RenderTexture(tempTexture);

        RenderTexture tempTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
        RenderTexture secondTempTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);

        Graphics.Blit(source, tempTexture);
        for (int i = 0; i < Iterations; i++)
        {
            blurMaterial.SetFloat("_Radius", BlurRadius);
            blurMaterial.SetTexture("_MainTex", tempTexture);
            Graphics.Blit(tempTexture, secondTempTexture, blurMaterial);
            blurMaterial.SetTexture("_MainTex", secondTempTexture);
            Graphics.Blit(secondTempTexture, tempTexture, blurMaterial);
        }
        vignetteMaterial.SetTexture("_MainTex", tempTexture);
        vignetteMaterial.SetFloat("_Intensity", Intensity);
        Graphics.Blit(tempTexture, destination, vignetteMaterial);

        RenderTexture.ReleaseTemporary(tempTexture);
        RenderTexture.ReleaseTemporary(secondTempTexture);
    }
    public void SetIntensity(float val)
    {
        Intensity = val;
    }
}
