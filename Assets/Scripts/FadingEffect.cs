﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class FadingEffect : MonoBehaviour
{
    [SerializeField]
    private Material NormalMat;
    [SerializeField]
    private Material FadingMaterial;
    [SerializeField, Range(0,50)]
    private float Threshold = 4;
    [SerializeField]
    private Camera CharacterCamera;

    public void SetCharacterCamera(Camera camera)
    {
        CharacterCamera = camera;
    }
    private Renderer Renderer;
    private void Start()
    {
        Renderer = GetComponent<Renderer>();
    }
    void FixedUpdate()
    {
        if(CharacterCamera == null)
        {
            return;
        }
        if(!Renderer.isVisible)
        {
            return;
        }
        if (Vector3.Distance(CharacterCamera.transform.position, transform.position) <= Threshold)
        {
            Renderer.material = FadingMaterial;
        }
        else
        {
            Renderer.material = NormalMat;
        }
    }
}
