﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
    [SerializeField]
    private float TotalEnery = 50;

    private float GrassAge = 0.01f; // 0 - 1
#if UNITY_EDITOR
    [ReadOnly, SerializeField]
#endif
    private float HitPoint = 100;

    private Agent Eater = null;

    private bool IsBeingEaten = false;

    public float Energy
    {
        get
        {
            return TotalEnery * (GrassAge);
        }
    }

    public bool SetEater(Agent eater)
    {
        if(Eater != null)
        {
            return false;
        }
        Eater = eater;
        IsBeingEaten = true;
        return true;
    }
    void Start()
    {
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    void FixedUpdate()
    {
        if (HitPoint <= 0)
        {
            Eater.FinishEating(this);
            gameObject.SetActive(false);
            Destroy(gameObject);
            return;
        }

        if (!IsBeingEaten)
        {
            transform.localScale = new Vector3(0.1f, 0.1f, 0.1f) * GrassAge;
            GrassAge += 0.5f * Time.fixedDeltaTime;
            GrassAge = Mathf.Min(GrassAge, 10);
        }
        else
        {
            HitPoint -= 5 * Time.fixedDeltaTime;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Tree"))
        {
            Destroy(gameObject);
        }
    }
}
