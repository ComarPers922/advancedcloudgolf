﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField]
    private PostProcessingCamera mainCamera;
    [SerializeField]
    private CharacterCamera characterCamera;
    [SerializeField, Range(0, 1)]
    private float DeltaPower = 0.01f;
    [SerializeField]
    private float Force = 50f;

    private Renderer BallRender;

    private float Power = 0;
    private Rigidbody rigidbody;
    private Vector3 cameraDir;

    private bool IsOnCloud = false;
    private bool IsOnTheGround = false;
 
    // Start is called before the first frame update
    void Start()
    {
        BallRender = GetComponentInChildren<Renderer>();
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float threshold = IsOnCloud ? 0.5f : 0.7f;
        characterCamera.IsIdle = !IsOnTheGround && rigidbody.velocity.magnitude < 0.5f && rigidbody.angularVelocity.magnitude < 0.5f;
        if(characterCamera.IsIdle)
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
        }
        if (Input.GetKey(KeyCode.T))
        {
            BallRender.material.SetInt("_IsTransparent", 1);
            gameObject.layer = LayerMask.NameToLayer("TransparentBall");
            foreach (Transform item in gameObject.transform)
            {
                item.gameObject.layer = LayerMask.NameToLayer("TransparentBall");
            }
        }
        if (Input.GetKey(KeyCode.Y))
        {
            BallRender.material.SetInt("_IsTransparent", 0);
            gameObject.layer = LayerMask.NameToLayer("Ball");
            foreach (Transform item in gameObject.transform)
            {
                item.gameObject.layer = LayerMask.NameToLayer("Ball");
            }
        }
        if (Input.GetMouseButton(0) && characterCamera.IsIdle)
        {
            characterCamera.IsAiming = true;
            Power += DeltaPower;
            Power = Mathf.Min(1, Power);
            cameraDir = characterCamera.gameObject.transform.forward;
        }
        else
        {
            rigidbody.AddForce(cameraDir * Power * Force);
            rigidbody.AddTorque(characterCamera.gameObject.transform.right * Power * Force);
            Power -= Time.fixedDeltaTime;
            Power = Mathf.Max(0, Power);
            characterCamera.IsAiming = false;
        }
        mainCamera.SetIntensity(Power);
        if(transform.position.y >= 0.5f && !IsOnCloud)
        {
            rigidbody.AddForce(Vector3.down * 200);
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if(collision.collider.gameObject.tag == "Cloud")
        {
            IsOnCloud = true;
            rigidbody.angularVelocity = rigidbody.angularVelocity - rigidbody.angularVelocity.normalized * 0.5f;
        }
        if(collision.collider.gameObject.tag == "Terrain")
        {
            IsOnTheGround = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        IsOnCloud = false;
        if (collision.collider.gameObject.tag == "Terrain")
        {
            IsOnTheGround = false;
        }
    }
}
