﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassGrower : MonoBehaviour
{
    [SerializeField]
    private FadingEffect[] Grasses;
    [SerializeField]
    private GameObject WorldOrigin;
    [SerializeField]
    private Camera CharacterCamera;
    void Start()
    {
        if(Grasses.Length == 0)
        {
            return;
        }
        for (int i = 0; i < 2000; i++)
        {
            var newGrass = Instantiate<FadingEffect>(Grasses[Random.Range(0, Grasses.Length)]);
            newGrass.SetCharacterCamera(CharacterCamera);
            newGrass.transform.position = WorldOrigin.transform.position + 
                                    new Vector3(Random.Range(-200f, 200f), 
                                    newGrass.transform.position.y, 
                                    Random.Range(-200, 200));           
        }
    }
}
