﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{
    public State CurrentState { private set; get; }
    public GenericState States { set; get; }

    private readonly HashSet<State> states = new HashSet<State>();
    public void UpdateStateMachine()
    {
        CurrentState.Update?.Invoke();
        CurrentState = CurrentState.CheckTransition() ?? CurrentState;
    }
    public void AddState(State newState)
    {
        if(states.Count == 0)
        {
            CurrentState = newState;
        }
        states.Add(newState);
    }
    public void AddTransition(State from, State to, StateTransition condition)
    {
        from.AddTransition(to, condition);
    }
}
