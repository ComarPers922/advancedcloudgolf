﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCamera : MonoBehaviour
{
    private const float MinDistance = 0;
    [SerializeField]
    private float RotationSpeed = 50.0f;
    [SerializeField]
    private GameObject Target;
    [SerializeField, Range(3, 200)]
    private float Distance = 100;
    [SerializeField]
    private float Height = 3.0f;
    [SerializeField]
    private float MouseScrollSensitivity = 50;
    private Vector3 offset;
    public bool IsIdle {set; get; } = true;
    public bool IsAiming { set; get; } = true;
    // Use this for initialization
    void Start()
    {
        offset = (transform.position - Target.transform.position).normalized * Distance;
        Cursor.lockState = CursorLockMode.Locked;
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBGL	
        Cursor.visible = false;
#endif
    }
    float ClampAngle(float angle, float min, float max)
    {
        if (angle < 0f) angle = 360 + angle;
        if (angle > 180f) return Mathf.Max(angle, 360 + min);
        return Mathf.Min(angle, max);
    }
    float AngleX = 0;
    // Update is called once per frame
    void FixedUpdate()
    {
        Distance -= Input.GetAxis("Mouse ScrollWheel") * MouseScrollSensitivity * Time.deltaTime;
        Distance = Mathf.Max(3, Distance);
        Distance = Mathf.Min(Distance, 100);
        transform.position = Vector3.MoveTowards(transform.position,
            Target.transform.position + (IsAiming ? offset.normalized * 0.2f + Vector3.up * 0.5f: offset), (IsAiming ? 100 : 60) * Time.deltaTime);
        if (IsIdle)
        {
            var mousePositionDelta = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
            transform.RotateAround(Target.transform.position,
                Vector3.up,
                mousePositionDelta.x * RotationSpeed * Time.deltaTime);
            AngleX -= mousePositionDelta.y * RotationSpeed * Time.deltaTime;
            AngleX = Mathf.Clamp(AngleX, -90, 90);
            var angle = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(AngleX, angle.y, angle.z);
        }
        else
        {
            var dir = Quaternion.LookRotation(Target.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, dir, 0.5f);
            AngleX = 0;
        }
        offset = (transform.position - Target.transform.position);
        offset -= offset.y * Vector3.up;
        offset.Normalize();
        offset = offset * Distance + Height * Vector3.up;
    }
}