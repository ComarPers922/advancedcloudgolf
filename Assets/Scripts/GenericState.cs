﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericStateTransition : StateTransition
{
    public StateTransitionConditionHandler CheckTransitionCondition { get; }
    public TransitionHandler DoTransition { get; }
    public GenericStateTransition(StateTransitionConditionHandler handler, 
                                    TransitionHandler transition)
    {
        CheckTransitionCondition = handler;
        DoTransition = transition;
    }
}
public class GenericState : State
{
    public string Name { get; }
    public StateUpdateHandler Update { get; }

    public Dictionary<StateTransition, State> NextStates { get; } = new Dictionary<StateTransition, State>();

    public void AddTransition(State to, StateTransition condition)
    {
        NextStates.Add(condition, to);
    }
    public State CheckTransition()
    {
        foreach (var item in NextStates)
        {
            if(item.Key?.CheckTransitionCondition?.Invoke() ?? false)
            {
                item.Key?.DoTransition?.Invoke();
                return item.Value;
            }
        }
        return null;
    }
    public GenericState (StateUpdateHandler newHandler, string name = "")
    {
        Update = newHandler;
        Name = name;
    }
}
