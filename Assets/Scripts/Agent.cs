﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.IO;
using UnityEditor;

#if UNITY_EDITOR
public class ReadOnlyAttribute : PropertyAttribute
{

}

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label)
    {
        GUI.enabled = false;
        EditorGUI.PropertyField(position, property, label, true);
        GUI.enabled = true;
    }
}
#endif
public class Agent : MonoBehaviour
{
#if UNITY_EDITOR
    [ReadOnly, SerializeField]
#endif
    private string CurrentState;

    [Header("Animal behavior"), Space(2)]
    [SerializeField, Range(0, 100)]
    private float Boredom;
    [SerializeField, Range(0, 500)]
    private float Energy;

    [SerializeField]
    private Collider GameArea;

    private static int Seed = 0;
    private static float Offset = 10f;

    private NavMeshAgent NavMeshAgent;
    private Animator Animator;

    private Vector3 Destination;

    private StateMachine Machine = new StateMachine();

    private Grass Food;

    private State IdleState;
    private State RunningState;
    private State EatingState;

    private StateTransition IdleToRunningTransition;
    private StateTransition RunningToIdleTransition;

    private StateTransition IdleToEatingTransition;
    private StateTransition EatingToIdleTransition;

    private StateTransition RunningToEatingTransition;

    private bool IsEating = false;

    public void FinishEating(Grass grass)
    {
        Energy += grass.Energy;
        IsEating = false;
    }

    void Start()
    {
        NavMeshAgent = GetComponent<NavMeshAgent>();
        Animator = GetComponent<Animator>();
        Random.InitState(Seed++);
        Destination = transform.position + new Vector3(Random.Range(-Offset, Offset), 0, Random.Range(-Offset, Offset));
        NextPosition();
        RunningState = new GenericState(delegate()
        {
            Boredom -= 0.1f;
            Energy -= 0.1f;
            if (!GameArea.bounds.Contains(Destination))
            {
                return;
            }
            NavMeshAgent.SetDestination(Destination);

            float dot = Vector2.Dot(new Vector2(transform.right.x,
                                        transform.right.z).normalized,
                                        new Vector2(NavMeshAgent.nextPosition.x - transform.position.x,
                                        NavMeshAgent.nextPosition.y - transform.position.y).normalized);
            Animator.SetFloat("AngularSpeed", Mathf.Abs(dot));
            Animator.SetBool("IsLeft", dot < 0);
        }, "RunningState");
        IdleState =  new GenericState(delegate()
        {
            Energy += Random.Range(0.1f, 0.3f);
            Boredom += Random.Range(0.1f, 0.3f);
        }, "IdleState");
        EatingState = new GenericState(delegate()
        {
            Animator.SetBool("IsEating", true);
            if (Food != null)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation,
                    Quaternion.LookRotation(Food.transform.position - transform.position,
                    Vector3.up), 10f);
            }
        }, "EatingState");

        IdleToRunningTransition = new GenericStateTransition(delegate()
        {
            return Boredom >= 40 && Energy >= 80;
        }, 
        delegate()
        {
            NextPosition();
            NavMeshAgent.isStopped = false;
        });
        RunningToIdleTransition = new GenericStateTransition(delegate()
        {
            return Energy <= 20 || Vector2.Distance(
                new Vector2(transform.position.x, transform.position.z),
                    new Vector2(Destination.x, Destination.z)) <= 1;
        },
        delegate()
        {
            NavMeshAgent.isStopped = true;
        });

        IdleToEatingTransition = new GenericStateTransition(delegate()
        {
            return IsEating;
        },
        delegate()
        {
            NavMeshAgent.isStopped = true;
        });
        RunningToEatingTransition = new GenericStateTransition(delegate()
        {
            return IsEating;
        },
        delegate()
        {
            NavMeshAgent.isStopped = true;
        });

        EatingToIdleTransition = new GenericStateTransition(delegate()
        {
            return !IsEating;
        },
        delegate()
        {
            Animator.SetBool("IsEating", false);
            NavMeshAgent.isStopped = false;
        });

        Machine.AddState(IdleState);
        Machine.AddState(RunningState);
        Machine.AddState(EatingState);

        Machine.AddTransition(IdleState, RunningState, IdleToRunningTransition);
        Machine.AddTransition(RunningState, IdleState, RunningToIdleTransition);

        Machine.AddTransition(IdleState, EatingState, IdleToEatingTransition);
        Machine.AddTransition(RunningState, EatingState, RunningToEatingTransition);

        Machine.AddTransition(EatingState, IdleState, EatingToIdleTransition);
    }

    // Update is called once per frame
    void Update()
    {
        Energy = Mathf.Clamp(Energy, 0, 500);
        Boredom = Mathf.Clamp(Boredom, 0, 100);
        CurrentState = Machine.CurrentState.Name;
        Machine.UpdateStateMachine();
        Animator.SetFloat("Speed", NavMeshAgent.velocity.magnitude);
    }
    private void NextPosition()
    {
        Destination = transform.position + 
            new Vector3(Random.Range(-Offset, Offset) * 5, 
            0, 
            Random.Range(-Offset, Offset) * 5);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Golf"))
        {
            Animator.SetTrigger("IsHurt");
        }
        if (collision.collider.CompareTag("Grass"))
        {
            if (Energy < 400)
            {
                Food = collision.collider.GetComponent<Grass>();
                IsEating = Food.SetEater(this);
            }
        }
    }
}
