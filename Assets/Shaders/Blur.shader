﻿Shader "Unlit/Blur"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Radius("Blur Radius", Range(0, 0.1)) = 0.0125
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
						#pragma vertex vert
						#pragma fragment frag

						#include "UnityCG.cginc"

						struct appdata
						{
							float4 vertex : POSITION;
							float2 uv : TEXCOORD0;
						};

						struct v2f
						{
							float2 uv : TEXCOORD0;
							float4 vertex : SV_POSITION;
						};

						sampler2D _MainTex;
						float4 _MainTex_ST;
						float _Radius;

						v2f vert(appdata v)
						{
							v2f o;
							o.vertex = UnityObjectToClipPos(v.vertex);
							o.uv = TRANSFORM_TEX(v.uv, _MainTex);
							return o;
						}

						fixed4 frag(v2f i) : SV_Target
						{
							float2 offsets[4] = { float2(0, -_Radius) ,
								float2(-_Radius, 0) ,                         float2(_Radius, 0),
													float2(0, _Radius) };
						const int length = 4;
							// sample the texture

							float4 col = tex2D(_MainTex, i.uv);
							for (int index = 0; index < length; index++)
							{
								col += tex2D(_MainTex, saturate(i.uv + offsets[index]));
							}
							col /= (length + 1);
						   return col;
					   }
					   ENDCG
				   }
	}
}
