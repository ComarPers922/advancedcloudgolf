﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/WindWithAlpha"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Threshold("Fade Threshold", Range(0, 50)) = 5

		_WindFrecuency("Wind Frecuency",Range(0.001,100)) = 1
		_WindStrength("Wind Strength", Range(0, 2)) = 0.3
		_WindGustDistance("Distance between gusts",Range(0.001,50)) = .25
		_WindDirection("Wind Direction", vector) = (1,0, 1,0)
	}
		SubShader
		{
			Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Opaque" }
			LOD 200
			ZWrite off
			CGPROGRAM
			#include "UnityCG.cginc"

			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows vertex:vert alpha:fade

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			sampler2D _MainTex;

			struct Input
			{
				float2 uv_MainTex;
				float3 worldPos;
			};
			half _Glossiness;
			half _Metallic;
			fixed4 _Color;

			half _WindFrecuency;
			half _WindGustDistance;
			half _WindStrength;
			float3 _WindDirection;
			float _Threshold;

			// our vert modification function
			void vert(inout appdata_full v)
			{
				float4 localSpaceVertex = v.vertex;
				// Takes the mesh's verts and turns it into a point in world space
				// this is the equivalent of Transform.TransformPoint on the scripting side
				float4 worldSpaceVertex = mul(unity_ObjectToWorld, localSpaceVertex);

				// Height of the vertex in the range (0,1)
				float height = (localSpaceVertex.y / 2 + .5);

				if (height > 0.6f)
				{
					worldSpaceVertex.x += sin(_Time.x * _WindFrecuency + worldSpaceVertex.x * _WindGustDistance) * height * _WindStrength * _WindDirection.x;
					worldSpaceVertex.z += sin(_Time.x * _WindFrecuency + worldSpaceVertex.z * _WindGustDistance) * height * _WindStrength * _WindDirection.z;
				}
				// takes the new modified position of the vert in world space and then puts it back in local space
				v.vertex = mul(unity_WorldToObject, worldSpaceVertex);
			}

			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				float camDist = distance(IN.worldPos, _WorldSpaceCameraPos);
				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				// Metallic and smoothness come from slider variables
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				if (camDist < _Threshold)
				{
					o.Alpha = 0.3f;
				}
				else
				{
					o.Alpha = c.a;
				}
			}
			ENDCG
		}
		FallBack "Diffuse"
}