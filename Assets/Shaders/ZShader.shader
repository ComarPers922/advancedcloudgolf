﻿Shader "Custom/ZShader"
{
    Properties
    {
        _Width("Width", Range(0, 10)) = 1
        _Intensity("Intensity", Range(0, 10)) = 1
        _HighlightColor("Highlight Color", Color) = (1,1,1,1)

        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _Glossiness("Smoothness", Range(0,1)) = 0.5
        _Metallic("Metallic", Range(0,1)) = 0.0
        _MainHighlightColor("Main Highlight Color", Color) = (1,1,1,1)
        _Ambient("Ambient", Color) = (1,1,1,1)
        [Toggle] _IsTransparent("Is Transparent", Int) = 0
    }
    SubShader
    {  
		Pass
        {
        ZWrite off
        ZTest Greater
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 500
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"

        struct v2f
        {
            float4 worldPos : SV_POSITION;
            float3 viewDir : TEXCOORD0;
            float3 worldNor : TEXCOORD1;
        };

        fixed4 _Color;
        fixed _Width;
        half _Intensity;
        fixed4 _HighlightColor;

        v2f vert(appdata_base v)
        {
            v2f o;
            o.worldPos = UnityObjectToClipPos(v.vertex);
            o.viewDir = normalize(WorldSpaceViewDir(v.vertex));
            o.worldNor = UnityObjectToWorldNormal(v.normal);

            return o;
        }

        float4 frag(v2f i) : SV_Target
        {
            _Width = 10 - _Width;
            half NDotV = saturate(dot(i.worldNor, i.viewDir));
            NDotV = pow(1 - NDotV, _Width) * _Intensity;

            fixed4 color;
            color.rgb = _HighlightColor.rgb;
            color.a = NDotV;
            return color;
        }

        ENDCG
        }
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
        CGPROGRAM
            // Physically based Standard lighting model, and enable shadows on all light types
            #pragma surface surf Standard fullforwardshadows alpha:fade keepalpha 

            // Use shader model 3.0 target, to get nicer looking lighting
            #pragma target 3.0

            sampler2D _MainTex;
            sampler2D _2ndaryTex;

            struct Input
            {
                float2 uv_MainTex;
                float4 worldPos;
                float3 viewDir;
                float3 worldNormal;
            };

            half _Glossiness;
            half _Metallic;
            fixed _Width;
            fixed _Intensity;
            fixed4 _Color;
            fixed4 _MainHighlightColor;
            float _IsTransparent;
            float _Ambient;

            void surf(Input IN, inout SurfaceOutputStandard o)
            {
                if (_IsTransparent <= 0.01)
                {
                    o.Albedo = _Color.rgb + _Ambient;
                    o.Alpha = _Color.a;
                }
                else
                {
                    _Width = 10 - _Width;
                    half NDotV = saturate(dot(IN.worldNormal, IN.viewDir));
                    NDotV = pow(1 - NDotV, _Width) * _Intensity;
                    fixed4 mainHighlightColor = _MainHighlightColor;
                    mainHighlightColor.a = NDotV;
                    mainHighlightColor.rgb *= NDotV;
                    mainHighlightColor.a = 1;
                    // Albedo comes from a texture tinted by color
                    fixed4 c = _Color * mainHighlightColor;
                    o.Albedo = c.rgb + _Ambient;
                    // Metallic and smoothness come from slider variables
                    o.Metallic = _Metallic;
                    o.Smoothness = _Glossiness;
                    o.Alpha = NDotV;
                }
            }
            ENDCG
    }
    FallBack "Diffuse"
}
