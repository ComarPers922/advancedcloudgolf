﻿Shader "Unlit/Circle"
{
    Properties
    {
		_Color("Color", Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
		_Width("Width", Range(0, 10)) = 1
		_Intensity("Intensity",Range(0, 10)) = 1
    }
    SubShader
    {
		Tags { "Queue" = "Transparent-100" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
		Cull off 
        ZWrite off
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float2 viewDir : TEXCOORD0;
                float4 worldPos : SV_POSITION;
				float3 worldNormal : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			half4 _Color;
			fixed _Width;
			half _Intensity;

            v2f vert (appdata_base  v)
            {
                v2f o;
                o.worldPos = UnityObjectToClipPos(v.vertex);
                o.viewDir = TRANSFORM_TEX(v.vertex, _MainTex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				half NDotV = saturate(dot(i.worldNormal, i.viewDir));
				NDotV = pow(1 - NDotV, 10 - _Width) * _Intensity;
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.worldPos);
			    col.rgb *= _Color.rgb;
				col.a =  _Color.a * NDotV;
                return col;
            }
            ENDCG
        }
    }
}
